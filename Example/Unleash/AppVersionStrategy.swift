//
//  AppVersionStrategy.swift
//  Unleash_Example
//
//  Created by Denis Dzyubenko on 18/07/2018.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import Unleash

class AppVersionStrategy: Strategy {
  var name: String { return "appVersion" }

  struct ConversionError: Error { }

  private func toVersionNumber(_ version: String) throws -> Int {
    let parts = try version
      .trimmingCharacters(in: CharacterSet(charactersIn: "+"))
      .split(separator: ".")
      .map { (partString: Substring) -> Int in
        guard let n = Int(String(partString)) else {
          throw ConversionError()
        }
        return n
    }

    let value = parts.enumerated().map({ (index, part) -> Decimal in
      let mul = 1000000 / pow(1000, index)
      return Decimal(part) * mul
    }).reduce(0, +)

    return NSDecimalNumber(decimal: value).intValue
  }

  func isEnabled(parameters: [String : ParameterValue], context: Unleash.Context) -> Bool {
    let actualAppVersion = context.properties?["appVersion"]?.trimmingCharacters(in: CharacterSet.whitespaces) ?? ""
    guard case var .string(minimumAppVersion)? = parameters["appVersion"] else {
      return false
    }

    let orNewer: Bool = minimumAppVersion.last == "+"
    if orNewer {
      minimumAppVersion.removeLast()
    }

    do {
      let minimumVersion = try toVersionNumber(minimumAppVersion)
      let actualVersion = try toVersionNumber(actualAppVersion)

      return orNewer ? actualVersion > minimumVersion : actualVersion == minimumVersion
    } catch {
      return false
    }
  }
}
