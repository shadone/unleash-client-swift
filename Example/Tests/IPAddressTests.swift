//
//  IPAddress.swift
//  UnleashTests
//
//  Created by Denis Dzyubenko on 17/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import XCTest
@testable import Unleash

class IPAddressTest: XCTestCase {
  func testWithoutNetmask() {
    XCTAssertEqual(IPAddress("10.1.2.3")!.address, 50462986)
    XCTAssertEqual(IPAddress("10.1.2.3")!.netmask, 0xFFFFFFFF)
  }

  func testWithNetmask() {
    XCTAssertEqual(IPAddress("10.1.2.3/32")!.netmask, 0xFFFFFFFF)
    XCTAssertEqual(IPAddress("10.1.2.3/24")!.netmask, 0xFFFFFF)
    XCTAssertEqual(IPAddress("10.1.2.3/16")!.netmask, 0xFFFF)
    XCTAssertEqual(IPAddress("10.1.2.3/8")!.netmask, 0xFF)

    XCTAssertNil(IPAddress("10.1.2.3/33"))
    XCTAssertNil(IPAddress("10.1.2.3/123"))
  }

  func testToString() {
    XCTAssertEqual(IPAddress("10.1.2.3/32")!.toString(), "10.1.2.3")
  }

  func testFirstAddress() {
    XCTAssertEqual(IPAddress("10.1.2.3/32")!.firstAddress.toString(), "10.1.2.3")
    XCTAssertEqual(IPAddress("10.1.2.3/24")!.firstAddress.toString(), "10.1.2.0")
  }

  func testLastAddress() {
    XCTAssertEqual(IPAddress("10.1.2.3/32")!.lastAddress.toString(), "10.1.2.3")
    XCTAssertEqual(IPAddress("10.1.2.3/24")!.lastAddress.toString(), "10.1.2.255")
    XCTAssertEqual(IPAddress("10.1.2.3/16")!.lastAddress.toString(), "10.1.255.255")
    XCTAssertEqual(IPAddress("10.1.2.3/8")!.lastAddress.toString(), "10.255.255.255")
  }

  func testContains() {
    XCTAssertTrue(IPAddress("10.1.2.3/24")!.contains(IPAddress("10.1.2.3")!))
    XCTAssertTrue(IPAddress("10.1.2.3/24")!.contains(IPAddress("10.1.2.0")!))
    XCTAssertTrue(IPAddress("10.1.2.3/24")!.contains(IPAddress("10.1.2.255")!))

    XCTAssertTrue(IPAddress("10.1.2.3/16")!.contains(IPAddress("10.1.2.3")!))
    XCTAssertTrue(IPAddress("10.1.2.3/16")!.contains(IPAddress("10.1.2.3/24")!))
  }
}
