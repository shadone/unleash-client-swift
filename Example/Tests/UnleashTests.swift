//
//  UnleashTests.swift
//  UnleashTests
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import XCTest
@testable import Unleash

struct ClientSpecification: Decodable {
  var name: String
  var state: State
  var tests: [Test]
}

extension ClientSpecification {
  struct State: Decodable {
    var version: Int
    var features: [FeatureDefinition]
  }

  struct Test: Decodable {
    var description: String
    var context: Unleash.Context
    var toggleName: String
    var expectedResult: Bool
  }
}

class ClientSpecificationReader {
  let all: [ClientSpecification]

  func spec(for name: String) -> ClientSpecification? {
    return all.first(where: { $0.name == name })
  }

  private init(specs: [ClientSpecification]) {
    self.all = specs
  }

  static func load() -> ClientSpecificationReader {
    let bundle = Bundle(for: ClientSpecificationReader.self)
    let indexPath = bundle.path(forResource: "index", ofType: "json")!
    let indexData = try! Data(contentsOf: URL(fileURLWithPath: indexPath))

    let decoder = JSONDecoder()
    let index = try! decoder.decode(Array<String>.self, from: indexData)

    let specs: [ClientSpecification] = index.map { nameWithExtension in
      let name = nameWithExtension.replacingOccurrences(of: ".json", with: "")
      let path = bundle.path(forResource: name, ofType: "json")!
      let data = try! Data(contentsOf: URL(fileURLWithPath: path))
      return try! decoder.decode(ClientSpecification.self, from: data)
    }

    return ClientSpecificationReader(specs: specs)
  }
}

class MockAPIClient: APIClient {
  var features: [FeatureDefinition] = []

  required init(baseUrl: String,
       customHeaders: [String:String],
       timeoutInterval: TimeInterval) {
  }

  func register(appName: String, instanceId: String, strategies: [String], started: Date, interval: Int, completion: @escaping (Error?) -> Void) {
    completion(nil)
  }
  func fetch(completion: @escaping ([FeatureDefinition]?, Error?) -> Void) {
    completion(features, nil)
  }
}

class UnleashTests: XCTestCase {
  let specs = ClientSpecificationReader.load()

  let unleash: Unleash = {
    Unleash.APIClientClass = MockAPIClient.self
    let unleash = Unleash(config: Unleash.Config(appName: "UnleashTests", url: "https://example.com"))
    unleash.start()
    return unleash
  }()

  var mockApiClient: MockAPIClient {
    return self.unleash.apiClient as! MockAPIClient
  }

  override func setUp() {
    Unleash.APIClientClass = MockAPIClient.self
  }

  func testAll() {
    print("🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄")
    self.specs.all.forEach { spec in
      print("\n\n➡️ Spec: \(spec.name)")
      self.unleash.safeSet(spec.state.features)
      spec.tests.forEach { test in
        print("\t⚙️ Testing: \(test.description)")
        let actualResult = self.unleash.isEnabled(test.toggleName, context: test.context)
        XCTAssertEqual(actualResult, test.expectedResult)
      }
    }
    print("🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄🍄")
  }

  func run(_ spec: ClientSpecification) {
    self.unleash.safeSet(spec.state.features)

    print("\n\n➡️ Spec: \(spec.name)")

    spec.tests.forEach { test in
      print("\t⚙️ Testing: \(test.description)")
      let actualResult = self.unleash.isEnabled(test.toggleName, context: test.context)
      XCTAssertEqual(actualResult, test.expectedResult)
    }
  }

  func test01SimpleExamples() {
    run(self.specs.spec(for: "01-simple-examples")!)
  }
  func test02UserWithIdStrategy() {
    run(self.specs.spec(for: "02-user-with-id-strategy")!)
  }
  func test03GradualRolloutUserIdStrategy() {
    run(self.specs.spec(for: "03-gradual-rollout-user-id-strategy")!)
  }
  func test04GradualRolloutSessionIdStrategy() {
    run(self.specs.spec(for: "04-gradual-rollout-session-id-strategy")!)
  }
  func test05GradualRolloutRandomStrategy() {
    run(self.specs.spec(for: "05-gradual-rollout-random-strategy")!)
  }
  func test06RemoteAddressStrategy() {
    run(self.specs.spec(for: "06-remote-address-strategy")!)
  }
  func test07MultipleStrategies() {
    run(self.specs.spec(for: "07-multiple-strategies")!)
  }
}
