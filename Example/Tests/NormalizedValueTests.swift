//
//  NormalizedValue.swift
//  UnleashTests
//
//  Created by Denis Dzyubenko on 17/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import XCTest
@testable import Unleash

class NormalizedValueTests: XCTestCase {
  func testExample() {
    XCTAssertEqual(normalizedValue("123", "gr1"), 73)
    XCTAssertEqual(normalizedValue("999", "groupX"), 25)
  }
}
