# Unleash

[![CI Status](https://img.shields.io/travis/Denis Dzyubenko/Unleash.svg?style=flat)](https://travis-ci.org/Denis Dzyubenko/Unleash)
[![Version](https://img.shields.io/cocoapods/v/Unleash.svg?style=flat)](https://cocoapods.org/pods/Unleash)
[![License](https://img.shields.io/cocoapods/l/Unleash.svg?style=flat)](https://cocoapods.org/pods/Unleash)
[![Platform](https://img.shields.io/cocoapods/p/Unleash.svg?style=flat)](https://cocoapods.org/pods/Unleash)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Unleash is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Unleash'
```

## Author

Denis Dzyubenko, denis@ddenis.info

## License

Unleash is available under the MIT license. See the LICENSE file for more info.
