//
//  UnleashError.swift
//  Unleash
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation

public enum UnleashError: Error {
  case invalidInput(String)
  case networkError(Error)
  case unexpectedStatusCode(String, Int)
  case JSONParseError(Error)
  case unsupportedApiVersion(Int)
}
