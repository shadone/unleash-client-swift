//
//  UnleashContext.swift
//  Unleash
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation

extension Unleash {
  public struct Context: Codable {
    public var userId: String? = nil
    public var sessionId: String? = nil
    public var remoteAddress: String? = nil
    public var properties: [String:String]? = nil

    public init(
      userId: String? = nil,
      sessionId: String? = nil,
      remoteAddress: String? = nil,
      properties: [String:String]? = nil) {
      self.userId = userId
      self.sessionId = sessionId
      self.remoteAddress = remoteAddress
      self.properties = properties
    }
  }
}
