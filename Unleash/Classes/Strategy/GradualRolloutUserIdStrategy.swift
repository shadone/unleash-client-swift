//
//  GradualRolloutUserIdStrategy.swift
//  Unleash
//
//  Created by Denis Dzyubenko on 17/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation

class GradualRolloutUserIdStrategy: Strategy {
  var name: String { return "gradualRolloutUserId" }

  func isEnabled(parameters: [String : ParameterValue], context: Unleash.Context) -> Bool {
    guard let userId = context.userId else {
      return false
    }

    let percentage: Int = {
      switch parameters["percentage"] {
      case let .number(value)?:
        return value
      case let .string(value)?:
        return Int(value) ?? 0
      case .none:
        return 0
      }
    }()

    let groupId: String = {
      if case let .string(value)? = parameters["groupId"] {
        return value
      } else {
        return ""
      }
    }()

    let normalizedUserId = normalizedValue(userId, groupId)
    return percentage > 0 && normalizedUserId <= percentage
  }
}
