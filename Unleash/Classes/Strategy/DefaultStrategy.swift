//
//  DefaultStrategy.swift
//  unleash-test
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted. All rights reserved.
//

import Foundation

class DefaultStrategy: Strategy {
  var name: String { return "default" }

  func isEnabled(parameters: [String : ParameterValue], context: Unleash.Context) -> Bool {
    return true
  }
}
