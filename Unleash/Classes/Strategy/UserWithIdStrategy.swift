//
//  UserWithIdStrategy.swift
//  unleash-test
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted. All rights reserved.
//

import Foundation

class UserWithIdStrategy: Strategy {
  var name: String { return "userWithId" }

  func isEnabled(parameters: [String : ParameterValue], context: Unleash.Context) -> Bool {
    guard let userId = context.userId, let userIdsValue = parameters["userIds"] else {
      return false
    }
    guard case let .string(userIdsString) = userIdsValue else {
      return false
    }

    let userIds = userIdsString.split(separator: ",")
      .map { $0.trimmingCharacters(in: CharacterSet.whitespaces) }

    return userIds.contains(userId)
  }
}
