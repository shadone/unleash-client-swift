//
//  GradualRolloutSessionIdStrategy.swift
//  Unleash
//
//  Created by Denis Dzyubenko on 17/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation

class GradualRolloutSessionIdStrategy: Strategy {
  var name: String { return "gradualRolloutSessionId" }

  func isEnabled(parameters: [String : ParameterValue], context: Unleash.Context) -> Bool {
    guard let sessionId = context.sessionId else {
      return false
    }

    let percentage: Int = {
      switch parameters["percentage"] {
      case let .number(value)?:
        return value
      case let .string(value)?:
        return Int(value) ?? 0
      case .none:
        return 0
      }
    }()

    let groupId: String = {
      if case let .string(value)? = parameters["groupId"] {
        return value
      } else {
        return ""
      }
    }()

    let normalizedId = normalizedValue(sessionId, groupId)

    return percentage > 0 && normalizedId <= percentage
  }
}
