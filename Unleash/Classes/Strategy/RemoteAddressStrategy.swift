//
//  RemoteAddressStrategy.swift
//  Unleash
//
//  Created by Denis Dzyubenko on 17/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation

struct IPAddress: Equatable {
  let address: UInt32
  let netmask: UInt32

  init(address: UInt32, netmask: UInt32) {
    self.address = address
    self.netmask = netmask
  }

  init?(_ address: String) {
    let chunks = address.split(separator: Character("/"))

    guard chunks.count <= 2 else {
      return nil
    }

    guard let addr = chunks.first else {
      return nil
    }

    let mask: String
    if let lastChunk = chunks.last {
      mask = String(lastChunk)
    } else {
      mask = "32"
    }

    self.address = addr.withCString { paddress -> UInt32 in
      var addrstruct = in_addr()
      let paddrstruct = UnsafeMutablePointer(&addrstruct)
      inet_aton(paddress, paddrstruct)
      return addrstruct.s_addr
    }

    if let nmask = UInt32(mask), nmask != 32 {
      if nmask > 32 {
        return nil
      }
      self.netmask = (UInt32(2) << (nmask - 1)) - 1
    } else {
      self.netmask = UInt32.max
    }
  }

  enum StringFormat {
    case ipAddressOnly
//    case ipAndNetmask
  }

  func toString(format: StringFormat = .ipAddressOnly) -> String {
    var addrstruct = in_addr()
    addrstruct.s_addr = self.address
    guard let cstring = inet_ntoa(addrstruct) else {
      return ""
    }
    return String(cString: cstring)
  }

  var firstAddress: IPAddress {
    let addr = self.address & self.netmask
    return IPAddress(address: addr, netmask: UInt32.max)
  }

  var lastAddress: IPAddress {
    let addr = (self.address & self.netmask) + (UInt32.max - self.netmask)
    return IPAddress(address: addr, netmask: UInt32.max)
  }

  func contains(_ target: IPAddress) -> Bool {
    return self.firstAddress.address <= target.firstAddress.address &&
      target.lastAddress.address <= self.lastAddress.address
  }
}

class RemoteAddressStrategy: Strategy {
  var name: String { return "remoteAddress" }

  func isEnabled(parameters: [String : ParameterValue], context: Unleash.Context) -> Bool {
    guard case let .string(ips)? = parameters["IPs"] else {
      return false
    }

    guard let remoteAddressString = context.remoteAddress,
      let remoteAddress = IPAddress(remoteAddressString) else {
      return false
    }

    let matchingIp = ips.split(separator: ",")
      .map { $0.trimmingCharacters(in: CharacterSet.whitespaces) }
      .first { rangeString -> Bool in
        guard let range = IPAddress(rangeString) else {
          return false
        }
        if range == remoteAddress {
          return true
        } else if range.contains(remoteAddress) {
          return true
        }
        return false
    }

    return matchingIp != nil
  }
}
