//
//  GradualRolloutRandomStrategy.swift
//  Unleash
//
//  Created by Denis Dzyubenko on 17/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation

class GradualRolloutRandomStrategy: Strategy {
  var name: String { return "gradualRolloutRandom" }

  func isEnabled(parameters: [String : ParameterValue], context: Unleash.Context) -> Bool {
    let percentage: Int

    switch parameters["percentage"] {
    case let .number(value)?:
      percentage = value
    case let .string(value)?:
      percentage = Int(value) ?? 0
    case .none:
      return true
    }

    let random = arc4random_uniform(100)
    return percentage >= random
  }
}
