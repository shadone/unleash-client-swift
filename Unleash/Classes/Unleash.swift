//
//  Unleash.swift
//  unleash-test
//
//  Created by Denis Dzyubenko on 12/07/2018.
//  Copyright © 2018 Schibsted. All rights reserved.
//

import Foundation

public enum ParameterValue: Decodable {
  case string(value: String)
  case number(value: Int)
}

extension ParameterValue {
  public init(from decoder: Decoder) throws {
    let container = try decoder.singleValueContainer()

    if let intValue = try? container.decode(Int.self) {
      self = .number(value: intValue)
      return
    }

    let stringValue = try container.decode(String.self)
    self = .string(value: stringValue)
  }
}

public class Unleash {
  enum RegistrationStatus {
    case none
    case inprogress
    case success
  }

  static internal var APIClientClass: APIClient.Type = APIClientImpl.self

  private var queue = DispatchQueue(label: "Unleash")
  internal var apiClient: APIClient
  internal var features: [String: FeatureDefinition]? = nil
  private var registrationStatus: RegistrationStatus = .none
  private var fetchTimer: Timer? = nil

  public let config: Config
  public weak var delegate: UnleashDelegate? = nil

  public init(config: Config) {
    self.config = config
    self.apiClient = Unleash.APIClientClass.init(
      baseUrl: self.config.url,
      customHeaders: self.config.customHeaders,
      timeoutInterval: min(self.config.refreshInterval, 30))
  }

  deinit {
    self.stop()
  }

  public func isEnabled(_ feature: String, context: Context? = nil, fallbackValue: Bool = false) -> Bool {
    guard let feature = self.safeGet(feature) else {
      return fallbackValue
    }
    if feature.enabled {
      let ctx = context ?? Context()
      if feature.strategies.isEmpty {
        return true
      }
      let successfullStrategy = feature.strategies.first { strategyData -> Bool in
        guard let strategy = self.getStrategy(name: strategyData.name) else {
          print("WARNING: Feature toggle '\(feature.name)' depends on an unknown strategy '\(strategyData.name)'")
          return false
        }
        return strategy.isEnabled(parameters: strategyData.parameters ?? [:], context: ctx)
      }
      return successfullStrategy != nil
    }
    return false
  }

  public func start() {
    if self.fetchTimer?.isValid ?? false {
      return
    }
    self.fetchTimer = Timer.scheduledTimer(
      withTimeInterval: self.config.refreshInterval,
      repeats: true,
      block: { [weak self] _ in
        self?.fetch()
    })
    // Trigger immediate fetch to avoid waiting for the timer to kick in
    self.fetch()
  }

  public func stop() {
    self.fetchTimer?.invalidate()
    self.fetchTimer = nil
  }

  private func register() {
    self.registrationStatus = .inprogress

    let strategies = self.config.strategies.map { $0.name }

    self.apiClient.register(
      appName: self.config.appName,
      instanceId: self.config.instanceId,
      strategies: strategies,
      started: Date(),
      interval: Int(self.config.metricInterval * 1000)) { [weak self] error in
        guard let strongSelf = self else { return }

        if let error = error {
          strongSelf.delegate?.unleash(strongSelf, didFailWithError: UnleashError.networkError(error))
          return
        }

        strongSelf.registrationStatus = .success
        // trigger immediate fetch to get feature toggles available asap
        strongSelf.fetch()
    }
  }

  private func fetch() {
    switch self.registrationStatus {
    case .none:
      self.register()
      return
    case .inprogress:
      return
    case .success:
      break
    }

    self.apiClient.fetch() { [weak self] features, error in
      guard let strongSelf = self else { return }

      if let error = error {
        strongSelf.delegate?.unleash(strongSelf, didFailWithError: error)
        return
      }

      strongSelf.safeSet(features ?? [])
    }
  }

  internal func safeGet(_ feature: String) -> FeatureDefinition? {
    return queue.sync { return self.features?[feature] }
  }

  internal func safeSet(_ features: [FeatureDefinition]) {
    var isFirstTimeSet = false

    let featuresDict = features.reduce(into: [String: FeatureDefinition]()) { $0[$1.name] = $1 }
    self.queue.sync(execute: DispatchWorkItem(qos: .background, flags: .barrier, block: {
      isFirstTimeSet = self.features == nil
      self.features = featuresDict
    }))

    if isFirstTimeSet {
      self.delegate?.unleashDidConnect(self)
    }
  }

  private func getStrategy(name: String) -> Strategy? {
    return self.config.strategies.first { $0.name == name }
  }
}
