//
//  Utils.swift
//  Unleash
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation
import MurmurHash3

internal func normalizedValue(_ id: String, _ groupId: String, _ normalizer: UInt32 = 100) -> UInt32 {
  let hashValue = MurmurHash3.hash32(key: "\(groupId):\(id)")
  return hashValue % normalizer + 1
}
